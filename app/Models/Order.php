<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";

    protected $fillable = [
        'username',
        'number',
        'deliver_address',
        'total_sum',
    ];
}
