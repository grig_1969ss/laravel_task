<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * @var array
     */
    public $additional = [
        'status' => 'success',
    ];
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'number' => $this->number,
            'deliver_address' => $this->deliver_address,
            'total_sum' => $this->total_sum,
            'created_at' => $this->updated_at,
        ];
    }
}
