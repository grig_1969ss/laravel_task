<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{
    /**
     * @var array
     */
    public $additional = [
        'status' => 'success',
    ];

    public $collects = "App\Http\Resources\OrderResource";
}
