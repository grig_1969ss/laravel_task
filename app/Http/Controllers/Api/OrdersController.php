<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Services\OrderService;
use Exception;
use Illuminate\Http\JsonResponse;

class OrdersController extends Controller
{

    /**
     * @var OrderService
     */
    private $service;

    /**
     * @param OrderService $service
     */
    public function __construct(OrderService $service)
    {
        $this->service = $service;
    }

    /**
     * @param int $id
     * @return OrderResource
     * @throws Exception
     */
    public function show(int $id): OrderResource
    {
        return $this->service->show($id);
    }

    /**
     * @return OrderCollection
     * @throws Exception
     */
    public function all():OrderCollection
    {
        return $this->service->all();
    }

    /**
     * @param OrderRequest $request
     * @return OrderResource
     * @throws Exception
     */
    public function create(OrderRequest $request): OrderResource
    {
        return $this->service->create($request->validated());
    }

    /**
     * @param OrderRequest $request
     * @param int $id
     * @return OrderResource
     * @throws Exception
     */
    public function update(OrderRequest $request, int $id): OrderResource
    {
        return $this->service->update($request->validated(),$id);
    }
}
