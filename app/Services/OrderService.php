<?php

namespace App\Services;

use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use Exception;

class OrderService
{
    /**
     * @param int $id
     * @return OrderResource
     * @throws Exception
     */
    public function show(int $id): OrderResource
    {
        try {
            return new OrderResource(Order::findOrFail($id));
        } catch (Exception $e) {
            throw new Exception("Order not found");
        }
    }

    /**
     * @return OrderCollection
     * @throws Exception
     */
    public function all(): OrderCollection
    {
        try {
            return new OrderCollection(Order::all());
        } catch (Exception $e) {
            throw new Exception("No Orders");
        }
    }

    /**
     * @param $request
     * @return OrderResource
     * @throws Exception
     */
    public function create($request): OrderResource
    {
        try {
            return new OrderResource(Order::create($request));
        } catch (Exception $e) {
            throw new Exception("Invalid data");
        }
    }

    /**
     * @param $request
     * @param int $id
     * @return OrderResource
     * @throws Exception
     */
    public function update($request, int $id): OrderResource
    {
        try {
            $order = Order::findOrFail($id);
            $order->update($request);
            return new OrderResource($order);
        } catch (Exception $e) {
            throw new Exception("Invalid data");
        }
    }
}
